// Définition de la classe Warrior et les propriétés dans le "constructor"
class Warrior {
  constructor(name, power, life) {
    this.name = name;
    this.power = power;
    this.life = life;
  }

  //Définition de la méthode "attack" pour attaquer un adversaire
  attack(opponent) {
    // Réduction de la vie de l'adversaire en fonction de la puissance du guerrier
    opponent.life -= this.power;
    console.log(
      `${this.name} attaque ${opponent.name} et lui inflige ${this.power} points de dégâts.`
    );
  }

  //Définition de la méthode "isAlive" afin de vérifier si le guerrier est toujours en vie
  isAlive() {
    //Condition qui renvoie "true" si la vie du guerrier est supérieure à 0, sinon cela renvoie "false"
    if (this.life > 0) {
      return true;
    } else return false;
  }
}
// Définition de la classe WarriorAxe qui est l'enfant de la classe parente Warrior
class WarriorAxe extends Warrior {
  // Redéfinition de la méthode "attack" pour les guerriers avec une hache
  attack(opponent) {
    // Condition = Si l'adversaire est de type WarriorSword, la puissance de l'attaque sera doublée
    if (opponent instanceof WarriorSword) {
      opponent.life -= this.power * 2;
      // Sinon, la conditon exécute l'attaque normale en utilisant la méthode de la classe parente Warrior
    } else {
      super.attack(opponent);
    }
  }
}

// Définition de la classe WarriorSword qui est l'enfant de la classe parente Warrior
class WarriorSword extends Warrior {
  // Redéfinition de la méthode "attack" pour les guerriers avec une épée
  attack(opponent) {
    // Condition = Si l'adversaire est de type WarriorSpear, la puissance de l'attaque sera doublée
    if (opponent instanceof WarriorSpear) {
      opponent.life -= this.power * 2;
      // Sinon, la conditon exécute l'attaque normale en utilisant la méthode de la classe parente Warrior
    } else {
      super.attack(opponent);
    }
  }
}

// Définition de la classe WarriorSpear qui est l'enfant de la classe parente Warrior
class WarriorSpear extends Warrior {
  // Redéfinition de la méthode "attack" pour les guerriers avec une lance
  attack(opponent) {
    // Condition = Si l'adversaire est de type WarriorAxe, la puissance de l'attaque sera doublée
    if (opponent instanceof WarriorAxe) {
      opponent.life -= this.power * 2;
      // Sinon, la conditon exécute l'attaque normale en utilisant la méthode de la classe parente Warrior
    } else {
      super.attack(opponent);
    }
  }
}

//Création des instances de chaque guerriers ainsi que leurs parametres
let ragnarLodbrok = new WarriorSword("Ragnar Lodbrok", 500, 1000);
let ivarRagnarsson = new WarriorSpear("Ivar Ragnarsson", 400, 1000);
let laguertha = new WarriorAxe("Laguertha", 600, 1000);

//Les attaques entre les guerriers
ragnarLodbrok.attack(ivarRagnarsson);
ivarRagnarsson.attack(laguertha);
laguertha.attack(ragnarLodbrok);

//Affichage dans le terminal pour le simulation d'attaque
console.log(`${ivarRagnarsson.name} a ${ivarRagnarsson.life} points de vie.`);
console.log(
  `${ivarRagnarsson.name} est-il encore en vie ? ${
    ivarRagnarsson.isAlive() ? "Oui" : "Non"
  }`
);

console.log(`${ragnarLodbrok.name} a ${ragnarLodbrok.life} points de vie. `);
console.log(
  `${ragnarLodbrok.name} est-il encore en vie ? ${
    ragnarLodbrok.isAlive() ? "oui" : "non"
  } `
);

console.log(`${laguertha.name} a ${laguertha.life} points de vie. `);
console.log(
  `${laguertha.name} est-il encore en vie ? ${
    laguertha.isAlive() ? "oui" : "non"
  } `
);

//Boucle qui simule un affrontement de deux guerriers, jusqu'à ce que l'un des deux guerriers n'a plus de vie
while (laguertha.isAlive() && ivarRagnarsson.isAlive()) {
  laguertha.attack(ivarRagnarsson);
  ivarRagnarsson.attack(laguertha);
}

//Condition pour determiner le résultat du combat
if (!laguertha.isAlive() && !ivarRagnarsson.isAlive()) {
  console.log("It's a draw.");
} else if (!laguertha.isAlive()) {
  console.log(`${ivarRagnarsson.name} win !`);
} else {
  console.log(`${laguertha.name} win !`);
}

export default Warrior;
